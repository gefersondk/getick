package utilities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import m7.fdots.getick.br.app.getick.R;


/**
 * Created by Geferson on 21/07/2015.
 */
public class NetWorkUtil {

    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 3;

    public static int getConnectivityType(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            int type = TYPE_NOT_CONNECTED;
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                type = TYPE_WIFI;

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                type = TYPE_MOBILE;

            if (isActive(activeNetwork)) {
                return type;
            } else {
                return TYPE_NOT_CONNECTED;
            }
        }
        return TYPE_NOT_CONNECTED;
    }

    private static boolean isActive(NetworkInfo activeNetwork) {
        return activeNetwork.isConnected() && activeNetwork.isAvailable();
    }

    public static boolean isConnected(Context context) {
        if (getConnectivityType(context) != TYPE_NOT_CONNECTED) {
            //Thread para poder acessar a internet.
            AsyncTask<Void, Void, Boolean> hasNetworkTask = new AsyncTask<Void, Void, Boolean>() {
                @Override
                protected Boolean doInBackground(Void... params) {
                    return hasActiveInternetConnection();
                }
            };

            try {
                return hasNetworkTask.execute().get();
            } catch (InterruptedException e) {
                Log.e("InterruptedException", e.getMessage());
                return false;
            } catch (ExecutionException e) {

                Log.e("ExectionException", e.getMessage());
                return false;
            }
        }
        return false;
    }

    //Checa se consegue se conectar a um site externo.
    public static boolean hasActiveInternetConnection() {
        try {
            HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
            urlc.setRequestProperty("User-Agent", "Test");
            urlc.setRequestProperty("Connection", "close");
            urlc.setConnectTimeout(1500);
            urlc.connect();

            return (urlc.getResponseCode() == 200);
        } catch (IOException e) {
            Log.e(e.getMessage(), "Error checking internet connection");
        }
        return false;
    }

    public static String getConnectivityTypeString(Context context) {
        int conn = NetWorkUtil.getConnectivityType(context);
        String status = null;
        if (conn == NetWorkUtil.TYPE_WIFI) {
            status = context.getString(R.string.wifi_connected);
        } else if (conn == NetWorkUtil.TYPE_MOBILE) {
            status = context.getString(R.string.mobile_connected);
        } else if (conn == NetWorkUtil.TYPE_NOT_CONNECTED) {
            status = context.getString(R.string.no_connection);
        }
        return status;
    }
}

