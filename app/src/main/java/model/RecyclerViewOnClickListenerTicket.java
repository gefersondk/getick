package model;

import android.view.View;

/**
 * Created by geferson on 10/11/2015.
 */
public interface RecyclerViewOnClickListenerTicket {

    public void onClickListener(View view, int position);
    public void onLongPressClickListener(View view, int position);

}
