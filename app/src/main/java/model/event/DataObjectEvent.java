package model.event;

/**
 * Created by geferson on 15/11/2015.
 */
public class DataObjectEvent {

    private int id;
    private String type;
    private Event attributes;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Event getAttributes() {
        return attributes;
    }

    public void setAttributes(Event attributes) {
        this.attributes = attributes;
    }

    @Override
    public String toString() {
        return "DataObjectEvent{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", attributes=" + attributes +
                '}';
    }
}
