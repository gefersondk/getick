package model.event;

import java.util.List;

/**
 * Created by geferson on 15/11/2015.
 */
public class DataEvent {

    private List<DataObjectEvent> data;
    private Links links;

    public List<DataObjectEvent> getData() {
        return data;
    }

    public void setData(List<DataObjectEvent> data) {
        this.data = data;
    }

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }

    @Override
    public String toString() {
        return "DataEvent{" +
                "data=" + data +
                ", links=" + links +
                '}';
    }
}
