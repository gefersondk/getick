package model.event;

/**
 * Created by geferson on 15/11/2015.
 */
public class Region {

    private City city;
    private State state;

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "Region{" +
                "city=" + city +
                ", state=" + state +
                '}';
    }
}
