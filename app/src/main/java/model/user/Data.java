package model.user;

import com.google.gson.annotations.SerializedName;

/**
 * Created by geferson on 03/01/2016.
 */
public class Data {

    private long id;
    private String type;
    @SerializedName("attributes")
    private User attributes;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public User getAttributes() {
        return attributes;
    }

    public void setAttributes(User attributes) {
        this.attributes = attributes;
    }

    @Override
    public String toString() {
        return "Data{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", attributes=" +
                '}';
    }
}
