package model.user;

/**
 * Created by geferson on 03/01/2016.
 */


public class UserGetick {


    private User user;

    public UserGetick(User user){
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
