package model.GetickNotification;

/**
 * Created by geferson on 08/12/2015.
 */
public class GetickNotification {

    private long id;
    private String title;
    private String description;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
