package model.config;

/**
 * Created by geferson on 07/12/2015.
 */
public class GetickConfig {

    private boolean receiveNotification;
    private String city;
    private String state;
    private boolean enableAnimation;

    public GetickConfig(){
        this.city = "";
        this.state = "";
        this.enableAnimation = false;
    }

    public boolean isReceiveNotification() {
        return receiveNotification;
    }

    public void setReceiveNotification(boolean receiveNotification) {
        this.receiveNotification = receiveNotification;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public boolean isEnableAnimation() {
        return enableAnimation;
    }

    public void setEnableAnimation(boolean enableAnimation) {
        this.enableAnimation = enableAnimation;
    }
}
