package fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import facada.GetickFacade;
import fragments.events.FragmentEvent;
import fragments.events.FragmentPreferences;
import m7.fdots.getick.br.app.getick.R;
import extras.SlidingTabLayout;
import model.config.GetickConfig;

/**
 * Created by geferson on 01/11/2015.
 */
public class FragmentContainer extends Fragment {

    private String name = "Getick";
    static final int NUM_ITEMS = 2;
    private MyAdapter mAdapter;
    private ViewPager mPager;
    private List<Fragment> fragments;
    private SlidingTabLayout tabs;
    private FragmentPreferences fragmentPreferences;
    private FragmentEvent fragmentEvent;
    private GetickConfig getickConfig;
    private boolean isChecked;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getickConfig = GetickFacade.getInstance().getGetickConfig();
        isChecked = getickConfig.isEnableAnimation();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_events, container, false);
        fragments = getFragments();
        mAdapter = new MyAdapter(getActivity().getSupportFragmentManager(), fragments, getActivity());

        mPager = (ViewPager)view.findViewById(R.id.pagerEvents);
        mPager.setAdapter(mAdapter);
        mPager.setOffscreenPageLimit(2);//quantidade de paginas mantidas para ambos os lados

        tabs = (SlidingTabLayout) view.findViewById(R.id.slidingTabLayoutTabs);
        tabs.setCustomTabView(R.layout.tv_tabs_view, R.id.tv_tab_view);
        tabs.setDistributeEvenly(true);//ocupar o mesmo tamanho em 100 da tela usar antes de setar o pager
        tabs.setBackgroundColor(getResources().getColor(R.color.colorPrimary));//colorPrimary
        tabs.setSelectedIndicatorColors(getResources().getColor(android.R.color.white));
        tabs.setViewPager(mPager);

        //TODO
        tabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    if (isChecked != getickConfig.isEnableAnimation()) {
                        isChecked = getickConfig.isEnableAnimation();
                        if (fragmentEvent != null)
                            fragmentEvent.loadEvents();
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    public List<Fragment> getFragments(){
        List<Fragment> fList = new ArrayList<Fragment>();

        fragmentEvent = new FragmentEvent();
        fragmentPreferences = new FragmentPreferences();

        fList.add(fragmentEvent);
        fList.add(fragmentPreferences);

        return fList;
    }

    public static class MyAdapter extends FragmentStatePagerAdapter {
        private List<Fragment> fragments;
        private Context context;
        private String titleTabs [] = {"Eventos","Preferências"};
        public MyAdapter(FragmentManager fm, List<Fragment> fragments, Context c) {
            super(fm);
            this.fragments = fragments;
            this.context = c;
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        @Override
        public Fragment getItem(int position) {
            //LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            //fragments.get(position).getView().setLayoutParams(params);
            return fragments.get(position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return this.titleTabs[position];
        }
    }

    @Override
    public String toString() {
        return name;
    }
}
