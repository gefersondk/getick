package fragments.events;

import android.net.Uri;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;
import adapterGetick.AdapterCardViewEvent;
import butterknife.Bind;
import butterknife.ButterKnife;
import facada.GetickFacade;
import m7.fdots.getick.br.app.getick.R;
import model.config.GetickConfig;
import model.event.DataEvent;
import model.event.DataObjectEvent;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import service.api.APIService;
import utilities.GetickRetrofitServiceConfig;

public class FragmentEvent extends Fragment {

    private String name = "Eventos";
    @Bind(R.id.progress_bar_event)
    protected ProgressBar progressBarEvent;
    @Bind(R.id.progress_bar_event_bottom)
    protected ProgressBar progressBarEventBottom;
    private List<DataObjectEvent> eventList;
    private AdapterCardViewEvent adapter;
    @Bind(R.id.card_list)
    protected RecyclerView recList;
    @Bind(R.id.swipeRefreshLayoutEvents)
    protected SwipeRefreshLayout swipeRefreshLayoutEvents;
    private int page, size = 20;
    private String next;
    private APIService service;
    protected Toolbar tbMain;
    private GetickConfig getickConfig;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        service = GetickRetrofitServiceConfig.getServiceConfig();
        eventList = new ArrayList<DataObjectEvent>();
        adapter = new AdapterCardViewEvent(getContext(),eventList, getActivity(), false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.activity_fragment_event, container, false);
        ButterKnife.bind(this, view);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        recList.setAdapter(adapter);
        swipeRefreshLayoutEvents.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //setRetainInstance(true);
        loadEvents();

        swipeRefreshLayoutEvents.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        loadEvents();
                    }
                });
            }
        });

        recList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager llm = (LinearLayoutManager) recList.getLayoutManager();

                if (eventList != null) {
                    if ((llm.findLastCompletelyVisibleItemPosition() + 1 == eventList.size()) && next != null) {
                        progressBarEventBottom.setVisibility(View.VISIBLE);
                        getSetEvents(++page, size);
                    }
                }
            }
        });

    }

    private void getSetEvents(int page, int size){

        if(service == null)
            service = GetickRetrofitServiceConfig.getServiceConfig();

        Call<DataEvent> dataEventCall = service.loadNextEvents(page, size);
        dataEventCall.enqueue(new Callback<DataEvent>() {
            @Override
            public void onResponse(Response<DataEvent> response, Retrofit retrofit) {

                DataEvent dataEvent = response.body();
                if (dataEvent != null) {

                    List<DataObjectEvent> dataObjectEvents = dataEvent.getData();

                    for (int i = 0; i < dataObjectEvents.size(); i++) {

                        AdapterCardViewEvent adapter = (AdapterCardViewEvent) recList.getAdapter();
                        adapter.addListItem(dataObjectEvents.get(i), eventList.size());//adiciona depois da ultima pois referencia a mesma do adapterRecyclerviewEvent
                    }
                    Log.i("verificar posicao ult", eventList.size() + "");
                    progressBarEventBottom.setVisibility(View.GONE);
                    next = dataEvent.getLinks().getNext();


                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });

    }

    public void loadEvents(){
        try{

            if(service == null)
                service = GetickRetrofitServiceConfig.getServiceConfig();

            page = 1;

            Call<DataEvent> dataEventCall = service.loadEvents();
            dataEventCall.enqueue(new Callback<DataEvent>() {
                @Override
                public void onResponse(final Response<DataEvent> response, Retrofit retrofit) {
                    final DataEvent dataEvent = response.body();
                    if(dataEvent != null){
                        eventList = dataEvent.getData();

                        AdapterCardViewEvent adapter = new AdapterCardViewEvent(getContext(), eventList, getActivity(),GetickFacade.getInstance().getGetickConfig().isEnableAnimation());
                        recList.setAdapter(adapter);
                        progressBarEvent.setVisibility(View.GONE);

                        next = dataEvent.getLinks().getNext();//iniciar o next

                        swipeRefreshLayoutEvents.setRefreshing(false);
                        progressBarEventBottom.setVisibility(View.GONE);//TODO
                    }
                }
                @Override
                public void onFailure(Throwable t) {

                    swipeRefreshLayoutEvents.setRefreshing(false);//TODO
                }
            });

        }catch (Exception e){

        }
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public void onPause() {
        super.onPause();
        //setRetainInstance(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
