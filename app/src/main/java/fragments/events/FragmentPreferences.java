package fragments.events;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.rey.material.widget.Switch;

import butterknife.Bind;
import butterknife.ButterKnife;
import facada.GetickFacade;
import m7.fdots.getick.br.app.getick.R;
import model.config.GetickConfig;

/**
 * Created by geferson on 05/12/2015.
 */
public class FragmentPreferences extends Fragment {
    private ArrayAdapter<String> adapterState;
    private ArrayAdapter<String> adapterCity;
    @Bind(R.id.id_spinner_state)
    protected Spinner spState;
    @Bind(R.id.id_spinner_city)
    protected Spinner spCity;
    @Bind(R.id.sw_animation)
    protected Switch swAnimation;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_fragment_event_preference, container, false);
        ButterKnife.bind(this, view);
        String state[] ={"Estado 1", "Estado 2", "Estado 3","Estado 4", "Estado 5", "Estado 6","Estado 7", "Estado 8", "Estado 9","Estado 10", "Estado 11", "Estado 12","Estado 13"};
        String city[] ={"Cidade 1", "Cidade 2", "Cidade 3","Cidade 4", "Cidade 5", "Cidade 6","Cidade 7", "Cidade 8", "Cidade 9","Cidade 10", "Cidade 11", "Cidade 12","Cidade 13"};
        adapterState = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item_state, state);
        adapterCity = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item_state, city);
        spState.setAdapter(adapterState);
        spCity.setAdapter(adapterCity);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        swAnimation.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(Switch aSwitch, boolean isChecked) {
                GetickFacade.getInstance().getGetickConfig().setEnableAnimation(isChecked);
            }
        });

    }
}
