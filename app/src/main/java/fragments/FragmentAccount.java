package fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.drawee.view.SimpleDraweeView;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;
import m7.fdots.getick.br.app.getick.R;
import utilities.Constants;

/**
 * Created by geferson on 17/11/2015.
 */
public class FragmentAccount extends Fragment {

    private String img_user;
    private String name = "Conta";
    @Bind(R.id.thumbnail_account)
    protected SimpleDraweeView thumbnail_account;
    @Bind(R.id.btn_sh_getick)
    protected Button btn_sh_getick;
    @Bind(R.id.imageButtonEdtUser)
    protected ImageView imageButtonEdtUser;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.account_fragment, container, false);
        ButterKnife.bind(this,view);


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        AccessToken accessToken = AccessToken.getCurrentAccessToken();

        if(accessToken != null){
            GraphRequest request = GraphRequest.newMeRequest(
                    accessToken,
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {
                            // Application code
                            //img_user = response.
                            Log.i("verificar response face", response.toString()+"");
                            Log.i("verificar response face", object.toString()+"");
                            if(object != null){
                                try {
                                    thumbnail_account.setImageURI(Uri.parse(String.format(Constants.AppConfig.THUMBNAIL_FB, object.get("id"))));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }else{
                                new Handler().post(new Runnable() {
                                    @Override
                                    public void run() {
                                        thumbnail_account.setImageDrawable(getResources().getDrawable(R.drawable.user));
                                    }
                                });

                            }

                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id, name, link");
            request.setParameters(parameters);
            request.executeAsync();
        }//"https://graph.facebook.com/862192253898333/picture?type=large"
        //else{
           // thumbnail_account.setImageDrawable(R.drawable.u);
        //}

        btn_sh_getick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try{
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "Getick App.");
                    //sendIntent.setPackage("com.whatsapp");
                    sendIntent.setType("text/plain");
                    startActivity(sendIntent);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });


    }

    @Override
    public String toString() {
        return name;
    }
}
