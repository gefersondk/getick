package fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.opengl.Visibility;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import adapterGetick.AdapterHome;
import adapterGetick.AdapterTickets;
import butterknife.Bind;
import butterknife.ButterKnife;
import m7.fdots.getick.br.app.getick.R;
import m7.fdots.getick.br.app.getick.ShowItemQrcode;
import model.RecyclerViewOnClickListenerTicket;
import model.Tickte;

/**
 * Created by geferson on 13/11/2015.
 */
public class FragmentHome extends android.support.v4.app.Fragment implements RecyclerViewOnClickListenerTicket {

    @Bind(R.id.id_home_news_recycler_view)
    protected RecyclerView tRecyclerView;
    private AdapterHome tAdapter;
    private RecyclerView.LayoutManager tLayoutManager;
    private List<Tickte> tickteList;
    //@Bind(R.id.swipeRefreshLayoutHome_News)TODO
    //protected SwipeRefreshLayout swipeRefreshLayoutHomeNews;
    @Bind(R.id.id_img_header_fragment_home)
    protected SimpleDraweeView img_header;

    @Bind(R.id.appbarHomeNews)
    protected AppBarLayout appBarLayout;

    private String name = "Getick";

    @Bind(R.id.collapsing_toolbar)
    protected CollapsingToolbarLayout collapsingToolbarLayout;

    @Bind(R.id.tb_home_fr)
    protected Toolbar tbHome;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tickteList = getSetHomeNews(9);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        ButterKnife.bind(this, view);

        //tbHome.inflateMenu(R.menu.menu_toolbar_old);

        tRecyclerView.setHasFixedSize(true);

        tRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager llm = (LinearLayoutManager) tRecyclerView.getLayoutManager();
                AdapterHome adapter = (AdapterHome) tRecyclerView.getAdapter();

                if (tickteList.size() < 27) {
                    if (tickteList.size() == llm.findLastCompletelyVisibleItemPosition() + 1) {//verificar se a ultima posição é do tamanho da lista, implica em ser o ultimo item que esta sendo exibido
                        List<Tickte> listAux = getSetHomeNews(5);

                        for (int i = 0; i < listAux.size(); i++) {
                            adapter.addListItem(listAux.get(i), tickteList.size());//adiciona depois da ultima pois referencia a mesma do adapterRecyclerview
                        }
                    }
                }
            }
        });

        //img_header.setImageURI(Uri.parse("http://25.media.tumblr.com/763c2e034c394d9e5acef850d69aa6c2/tumblr_mjajgg99NL1r9771lo1_500.gif"));
        //http://25.media.tumblr.com/763c2e034c394d9e5acef850d69aa6c2/tumblr_mjajgg99NL1r9771lo1_500.gif
        Uri uri;
        String imgs [] = {"http://data.whicdn.com/images/70534525/large.gif",
                "https://c1.staticflickr.com/3/2753/4344019581_89cd1751ed_z.jpg?zz=1","https://external-gru2-1.xx.fbcdn.net/safe_image.php?d=AQCQbTXS8sQ116xz&w=470&h=246&url=https%3A%2F%2Fwww.facebook.com%2Fads%2Fimage%2F%3Fd%3DAQJr5DtZrrHsvg1nfprqMvb672LHnNqj-U7Y5ieFlnAyGRpSWXSwl5GG2kNh-EvO-_dgFaSZ_dpVV3WppmodwD1zbHWmhboYUP5dbME49EwIcb7TCUuZnef50hFhB0PD0wUw3lgkgwouM8gMaOOI_CFy&cfs=1&upscale=1&sx=0&sy=0&sw=1200&sh=628&l"};
        Random r = new Random();
        final int p = r.nextInt(3);

        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setUri(Uri.parse(imgs[p]))
                .setTapToRetryEnabled(true)
                .setAutoPlayAnimations(true).build();
        img_header.setController(controller);
        //mSimpleDraweeView.setController(controller);
        collapsingToolbarLayout.setTitle(toString());
        //swipeRefreshLayoutHomeNews.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));TODO

        tLayoutManager = new LinearLayoutManager(getActivity());
        //tLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        tRecyclerView.setLayoutManager(tLayoutManager);
        tRecyclerView.addOnItemTouchListener(new RecyclerViewTouchListener(getActivity(), tRecyclerView, (RecyclerViewOnClickListenerTicket) this));

        for(int i = 0; i< 10; i++){
            tickteList.add(new Tickte());
        }

        tAdapter = new AdapterHome(getActivity().getApplicationContext(), this.tickteList, getActivity());

        tRecyclerView.setAdapter(tAdapter);


//        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
//            @Override
//            public void onOffsetChanged(AppBarLayout appBarLayout, int offset) {
//                if (offset == 0){
//                    new Handler().post(new Runnable() {
//                        @Override
//                        public void run() {
//                            tbHome.getMenu().setGroupVisible(0, false);
//                        }
//                    });
//                    // Collapsed
//                }
//                else{
//                    // Not collapsed
//                    new Handler().post(new Runnable() {
//                        @Override
//                        public void run() {
//                            //tbHome.getMenu().findItem(R.id.item_menu_notification).setVisible(true);
//                            tbHome.getMenu().setGroupVisible(0, true);
//                        }
//                    });
//                }
//            }
//        });


        return view;
    }

//    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {TODO
//        super.onActivityCreated(savedInstanceState);
//        setRetainInstance(true);
//        swipeRefreshLayoutHomeNews.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        try {
//
//                        } catch (Exception e) {
//
//                        }
//                        swipeRefreshLayoutHomeNews.setRefreshing(false);
//                    }
//                }, 3000);
//            }
//        });
//
//
//    }


//    @Override
//    public void onStart() {
//        super.onStart();
//        img_header.setImageURI(Uri.parse("https://c1.staticflickr.com/3/2753/4344019581_89cd1751ed_z.jpg?zz=1"));
//    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    private List<Tickte> getSetHomeNews(int qnt){
        List<Tickte> tickteListAux = new ArrayList<Tickte>();
        for(int i = 0; i< qnt; i++){
            tickteListAux.add(new Tickte());
        }
        return tickteListAux;
    }

    @Override
    public void onClickListener(View view, int position) {
        //Toast.makeText(getActivity(), "click", Toast.LENGTH_SHORT).show();
//        Bundle bundle = new Bundle();
//        bundle.putInt("position", position);
//        startActivity(new Intent(getActivity(), ShowItemQrcode.class).putExtras(bundle));
//        AdapterHome adapterRecyclerview = (AdapterHome) tRecyclerView.getAdapter();
    }

    @Override
    public void onLongPressClickListener(View view, int position) {
        //Toast.makeText(getActivity(), "Long click", Toast.LENGTH_SHORT).show();
    }

    private static class RecyclerViewTouchListener implements RecyclerView.OnItemTouchListener{
        private Context context;
        private GestureDetector gestureDetector;
        RecyclerViewOnClickListenerTicket recyclerViewOnClickListenerTicket;

        public RecyclerViewTouchListener(Context c, final RecyclerView rv, RecyclerViewOnClickListenerTicket rVOnCLContact){
            this.context = c;
            this.recyclerViewOnClickListenerTicket = rVOnCLContact;
            this.gestureDetector = new GestureDetector(c, new GestureDetector.SimpleOnGestureListener(){
                @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);
                    View v = rv.findChildViewUnder(e.getX(),e.getY());
                    if(v != null && recyclerViewOnClickListenerTicket != null){
                        recyclerViewOnClickListenerTicket.onLongPressClickListener(v, rv.getChildPosition(v));//pegar posição da view
                    }
                }

                @Override
                public boolean onSingleTapUp(MotionEvent e) {

                    View v = rv.findChildViewUnder(e.getX(),e.getY());
                    if(v != null && recyclerViewOnClickListenerTicket != null){
                        recyclerViewOnClickListenerTicket.onClickListener(v, rv.getChildPosition(v));//pegar posição da view
                    }

                    return true;
                }
            });

        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            gestureDetector.onTouchEvent(e);
            //Toast.makeText(context, "onInterceptTouchEvent",Toast.LENGTH_SHORT).show();
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
            //Toast.makeText(context, "onTouchEvent",Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
            //Toast.makeText(context, "onRequestDisallowInterceptTouchEvent",Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public String toString() {
        return name;
    }
}
