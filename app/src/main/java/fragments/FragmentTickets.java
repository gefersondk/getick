package fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;

import adapterGetick.AdapterTickets;
import butterknife.Bind;
import butterknife.ButterKnife;
import m7.fdots.getick.br.app.getick.R;
import m7.fdots.getick.br.app.getick.ShowItemQrcode;
import model.RecyclerViewOnClickListenerTicket;
import model.Tickte;


public class FragmentTickets extends android.support.v4.app.Fragment implements RecyclerViewOnClickListenerTicket {

    @Bind(R.id.ticktes_recycler_view)
    protected RecyclerView tRecyclerView;
    private AdapterTickets tAdapter;
    private RecyclerView.LayoutManager tLayoutManager;
    private List<Tickte> tickteList;
    @Bind(R.id.swipeRefreshLayoutTickets)
    protected SwipeRefreshLayout swipeRefreshLayoutTickets;
    private String name = "Tickets";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tickteList = getSetTickets(9);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tickets, container, false);

        ButterKnife.bind(this, view);

        tRecyclerView.setHasFixedSize(true);

        tRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager llm = (LinearLayoutManager) tRecyclerView.getLayoutManager();
                AdapterTickets adapter = (AdapterTickets) tRecyclerView.getAdapter();

                if (tickteList.size() < 27) {
                    if (tickteList.size() == llm.findLastCompletelyVisibleItemPosition() + 1) {//verificar se a ultima posição é do tamanho da lista, implica em ser o ultimo item que esta sendoe exibido
                        List<Tickte> listAux = getSetTickets(5);

                        for (int i = 0; i < listAux.size(); i++) {
                            adapter.addListItem(listAux.get(i), tickteList.size());//adiciona depois da ultima pois referencia a mesma do adapterRecyclerviewContacts
                        }
                    }
                }
            }
        });

        swipeRefreshLayoutTickets.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));

        tLayoutManager = new LinearLayoutManager(getActivity());
        //tLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        tRecyclerView.setLayoutManager(tLayoutManager);
        tRecyclerView.addOnItemTouchListener(new RecyclerViewTouchListener(getActivity(), tRecyclerView, (RecyclerViewOnClickListenerTicket) this));

        for(int i = 0; i< 10; i++){
            tickteList.add(new Tickte());
        }

        tAdapter = new AdapterTickets(getActivity().getApplicationContext(), this.tickteList, getActivity());

        tRecyclerView.setAdapter(tAdapter);



        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
        swipeRefreshLayoutTickets.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        swipeRefreshLayoutTickets.setRefreshing(false);
                    }
                }, 3000);
            }
        });


    }



    private List<Tickte> getSetTickets(int qnt){
        List<Tickte> tickteListAux = new ArrayList<Tickte>();
        for(int i = 0; i< qnt; i++){
            tickteListAux.add(new Tickte());
        }
        return tickteListAux;
    }

    @Override
    public void onClickListener(View view, int position) {
        //Toast.makeText(getActivity(), "click", Toast.LENGTH_SHORT).show();
        Bundle bundle = new Bundle();
        bundle.putInt("position", position);
        startActivity(new Intent(getActivity(), ShowItemQrcode.class).putExtras(bundle));
        AdapterTickets adapterRecyclerview = (AdapterTickets) tRecyclerView.getAdapter();
    }

    @Override
    public void onLongPressClickListener(View view, int position) {
        Toast.makeText(getActivity(), "Long click", Toast.LENGTH_SHORT).show();
    }

    private static class RecyclerViewTouchListener implements RecyclerView.OnItemTouchListener{
        private Context context;
        private GestureDetector gestureDetector;
        RecyclerViewOnClickListenerTicket recyclerViewOnClickListenerTicket;

        public RecyclerViewTouchListener(Context c, final RecyclerView rv, RecyclerViewOnClickListenerTicket rVOnCLContact){
            this.context = c;
            this.recyclerViewOnClickListenerTicket = rVOnCLContact;
            this.gestureDetector = new GestureDetector(c, new GestureDetector.SimpleOnGestureListener(){
                @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);
                    View v = rv.findChildViewUnder(e.getX(),e.getY());
                    if(v != null && recyclerViewOnClickListenerTicket != null){
                        recyclerViewOnClickListenerTicket.onLongPressClickListener(v, rv.getChildPosition(v));//pegar posição da view
                    }
                }

                @Override
                public boolean onSingleTapUp(MotionEvent e) {

                    View v = rv.findChildViewUnder(e.getX(),e.getY());
                    if(v != null && recyclerViewOnClickListenerTicket != null){
                        recyclerViewOnClickListenerTicket.onClickListener(v, rv.getChildPosition(v));//pegar posição da view
                    }

                    return true;
                }
            });

        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            gestureDetector.onTouchEvent(e);
            //Toast.makeText(context, "onInterceptTouchEvent",Toast.LENGTH_SHORT).show();
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
            //Toast.makeText(context, "onTouchEvent",Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
            //Toast.makeText(context, "onRequestDisallowInterceptTouchEvent",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public String toString() {
        return name;
    }
}
