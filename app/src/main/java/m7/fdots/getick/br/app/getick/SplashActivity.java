package m7.fdots.getick.br.app.getick;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import butterknife.Bind;
import butterknife.ButterKnife;
import utilities.Constants;


public class SplashActivity extends Activity implements Runnable{//implements Runnable

    //@Bind(R.id.imageViewSplash)
    //protected ImageView img_splash;

    @Bind(R.id.title_splash)
    protected TextView title;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        try{
            YoYo.with(Techniques.BounceInDown)//BounceInDown RubberBand
                    .duration(1900)
                    .playOn(title);

        }catch(Exception e){
            e.printStackTrace();
        }

//        final Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.getick_anim);
//        //final Animation animationOut = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_out);
//        img_splash.startAnimation(animation);
//
//        animation.setAnimationListener(new Animation.AnimationListener() {
//            @Override
//            public void onAnimationStart(Animation animation) {
//
//            }
//
//            @Override
//            public void onAnimationEnd(Animation animation) {
//                //img_splash.startAnimation(animationOut);
//                finish();
//                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
//
//            }
//
//            @Override
//            public void onAnimationRepeat(Animation animation) {
//
//            }
//        });

        Handler h = new Handler();
        //solicita para o Handler executar o Runnable (this), fechando a Splash Screen
        //depois de alguns segundos
        h.postDelayed(this, Constants.AppConfig.DELAY);
    }

    @Override
    public void run() {
        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
        finish();
    }


}
