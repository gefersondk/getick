package m7.fdots.getick.br.app.getick;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by geferson on 08/12/2015.
 */
public class GetickNotificationActivity extends AppCompatActivity {

    private String title = "Notificações";

    @Bind(R.id.main_toolbar_notifications)
    protected Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_getick_notification);
        ButterKnife.bind(this);

        if(toolbar != null){
            toolbar.setTitle(title);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        int id = item.getItemId();
        if(id == android.R.id.home){
            finish();
//        }else{
//            try{
//                startActivity(new Intent(this,LoginActivity.class));
//                finish();
//            }catch (Exception e){
//
//            }
//
       }
        return true;
    }
}
