package m7.fdots.getick.br.app.getick;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.UiThread;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONObject;

import java.util.Arrays;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by geferson on 31/10/2015.
 */
public class LoginActivity extends Activity {

    private static String TAG = "LoginActivity";
    @Bind(R.id.btn_login)
    protected Button btnLogin;

    @Bind(R.id.editTextEmail)
    protected EditText email;

    @Bind(R.id.editTextPassword)
    protected EditText password;

    @Bind(R.id.name_app_login)
    protected TextView name;

    @Bind(R.id.btn_sign_up)
    protected Button btn_sign_up;

    @Bind(R.id.container_form_login)
    protected LinearLayout l_form_login;
    protected CallbackManager callbackManager;

   // private ProgressDialog dialog;
    private MaterialDialog dialog;

    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;
    private Context context;
    @Bind(R.id.login_button)
    protected LoginButton loginButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this.getApplicationContext());

        setContentView(R.layout.form_login);
        ButterKnife.bind(this);
        context = this.getApplicationContext();

        Typeface face= Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
        email.setTypeface(face);
        password.setTypeface(face);
        name.setTypeface(face);
        btnLogin.setTypeface(face);
        btn_sign_up.setTypeface(face);

        if(isLoggedIn()){
            //startActivity(new Intent(this, MainActivity.class));
            //finish();
        }


        callbackManager = CallbackManager.Factory.create();
        //LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
        //loginButton.setReadPermissions("user_friends");
        loginButton.setReadPermissions(Arrays.asList("public_profile, email, user_birthday, user_friends"));
        callbackManager = CallbackManager.Factory.create();

        loginButton.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    private ProfileTracker mProfileTracker;
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        AccessToken accessToken = loginResult.getAccessToken();
                        Profile profile = Profile.getCurrentProfile();


                        //startActivity(new Intent(LoginActivity.this, MainActivity.class));

                        Log.i("verificar token", accessToken.getToken() + "");

//                        ProfileTracker profileTracker = new ProfileTracker() {
//                            @Override
//                            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
//                                this.stopTracking();
//                                Profile.setCurrentProfile(currentProfile);
//
//
//                            }
//                        };
//                        profileTracker.startTracking();
//                        if(Profile.getCurrentProfile() != null) {
//                            Profile profile = Profile.getCurrentProfile();
//                            Log.i("verificar token", profile.getFirstName() + "");
//                        }else{
//                            profileTracker.startTracking();
//                        }

                        // App code
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(
                                            JSONObject object,
                                            GraphResponse response) {
                                        // Application code
                                        Log.v("LoginActivity", response.toString());
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,last_name,email,gender, birthday");
                        request.setParameters(parameters);
                        request.executeAsync();

                        if(profile != null)
                            Log.i("verificar token", profile.toString()+"");

                    }

                    @Override
                    public void onCancel() {
                        //
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        //
                    }
                });

        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken old, AccessToken newToken) {
                if(old != null)
                    Log.i("verificar token", old.getToken());
                if(newToken != null)
                    Log.i("verificar token", newToken.getToken()+"");
            }
        };

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {

            }
        };

        accessTokenTracker.startTracking();
        profileTracker.startTracking();

        btn_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, CreateAccountActivity.class));
            }
        });



    }



    @OnClick(R.id.btn_login)
    protected void login(View view){
        //int color = Color.TRANSPARENT;
        //Drawable background = view.getRootView().getBackground();
        //if (background instanceof ColorDrawable)
            //color = ((ColorDrawable) background).getColor();
        //Log.i("valor da cor", color + "");
        //Log.i("valor da cor", background.toString());
       // Log.i("valor da cor", background+"");

        new AsyncTask<Void,Void,Void>(){
            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                //dialog = ProgressDialog.show(LoginActivity.this, "Getick", "Conectando", true);
                dialog = new MaterialDialog.Builder(LoginActivity.this)
                        .title("Getick")
                        .content("Conectando")
                        .progress(true, 0)
                        .show();
            }

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                startActivity(new Intent(context, MainActivity.class));
                finish();
                dialog.dismiss();
            }
        }.execute();
    }

    public boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        accessTokenTracker.stopTracking();
        profileTracker.stopTracking();
    }
}
