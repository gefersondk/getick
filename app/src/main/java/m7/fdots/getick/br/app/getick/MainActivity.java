package m7.fdots.getick.br.app.getick;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;


import butterknife.Bind;
import butterknife.ButterKnife;
import enumFragment.FragmentSw;
import fragments.FragmentHome;
import gcm.QuickstartPreferences;
import gcm.RegistrationIntentService;

public class MainActivity extends AppCompatActivity {


    private static final String TAG = "MainActivity";
    public final String STATE_SELECTED = "state_selected";
    @Bind(R.id.main_toolbar)
    protected Toolbar tbMain;

    @Bind(R.id.inc_toolbar_bottom)
    protected Toolbar tbBottom;

    private Context context;
    private FragmentManager fragmentManager;
    private Fragment fragment;
    private FragmentSw sw;
    private ImageButton imgButtom;

    @Bind(R.id.id_ticket)
    ImageButton btStart;


    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;


    //private BroadcastReceiver mRegistrationBroadcastReceiver;
    //private ProgressBar mRegistrationProgressBar;
    //private TextView mInformationTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        Fresco.initialize(this);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        this.context = getApplicationContext();

        this.tbMain.setTitle(getResources().getString(R.string.app_name));
        this.tbMain.inflateMenu(R.menu.menu_toolbar);
        this.tbMain.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.item_menu_notification:
                        startActivity(new Intent(context, GetickNotificationActivity.class));
                        return true;
                }

                return false;
            }
        });


//runOnUiThread(new Runnable() {

//        this.tbBottom.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem item) {
//
//
//                return true;
//            }
//        });
//        this.tbBottom.inflateMenu(R.menu.menu_getick);



        this.fragmentManager = getSupportFragmentManager();



        if(savedInstanceState == null){
            this.fragment = new FragmentHome();
            fragmentManager.beginTransaction().replace(R.id.container,fragment).commit();
            //btStart.setBackgroundColor(getResources().getColor(R.color.blue));
            //btStart.setSelected(true);
            imgButtom = btStart;

        }


//        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                //mRegistrationProgressBar.setVisibility(ProgressBar.GONE);
//                SharedPreferences sharedPreferences =
//                        PreferenceManager.getDefaultSharedPreferences(context);
//                boolean sentToken = sharedPreferences
//                        .getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
//                if (sentToken) {
//                    //mInformationTextView.setText(getString(R.string.gcm_send_message));
//                    //Log.i("GCM Registration Token: " + );
//                } else {
//                    //mInformationTextView.setText(getString(R.string.token_error_message));
//                }
//            }
//        };

        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }

    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_toolbar_old, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//        Toast.makeText(context,"teste",Toast.LENGTH_SHORT).show();
//        return super.onOptionsItemSelected(item);
//    }


    public void replaceFragment(View view){

        sw = FragmentSw.valueOf((String) view.getTag());
        fragment = sw.getFragment();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        //fragmentTransaction.addToBackStack("pilha");
        fragmentTransaction.commit();

        if(view.getTag().equals("home_news"))//|| view.getTag().equals("events")
            tbMain.setVisibility(View.GONE);

        else{
            tbMain.setVisibility(View.VISIBLE);
            tbMain.setTitle(fragment.toString());
            //tbMain.setLogo(R.drawable.ic_audio_effects);
        }

        //if(imgButtom != null)
            //imgButtom.setBackgroundColor(getResources().getColor(R.color.colorGetick));
        //ImageButton bt = (ImageButton) view;
        //bt.setBackgroundColor(getResources().getColor(R.color.blue));
        //imgButtom = bt;
    }


    @Override
    protected void onResume() {
        super.onResume();
//        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
//                new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
    }

    @Override
    protected void onPause() {
        super.onPause();
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
//        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //outState.putBoolean(STATE_SELECTED, imgButtom.isSelected());
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
}
