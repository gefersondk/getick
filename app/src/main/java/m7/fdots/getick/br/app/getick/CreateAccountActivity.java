package m7.fdots.getick.br.app.getick;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;
import model.user.Data;
import model.user.User;
import model.user.UserGetick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import service.api.APIService;
import utilities.GetickRetrofitServiceConfig;

/**
 * Created by geferson on 15/11/2015.
 */
public class CreateAccountActivity extends Activity{

    @Bind(R.id.btn_confirm_createAccount)
    protected Button create;

    @Bind(R.id.editFirst_name)
    protected MaterialEditText first_name;

    @Bind(R.id.editLast_name)
    protected MaterialEditText last_name;

    @Bind(R.id.editPassword)
    protected MaterialEditText password;

    @Bind(R.id.editPassword_confirmation)
    protected MaterialEditText password_confirmation;

    @Bind(R.id.editEmail)
    protected MaterialEditText email;


    @Bind(R.id.editPhone_number)
    protected MaterialEditText phone_number;

    private APIService service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        ButterKnife.bind(this);

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user = new User(first_name.getText().toString(), last_name.getText().toString(), email.getText().toString() , phone_number.getText().toString(), password.getText().toString(),password_confirmation.getText().toString());
                UserGetick userGetick = new UserGetick(user);
                service = GetickRetrofitServiceConfig.getServiceConfig();

                Call<Data> userCall = service.createUser(userGetick);
                userCall.enqueue(new Callback<Data>() {
                    @Override
                    public void onResponse(Response<Data> response, Retrofit retrofit) {
                        Log.i("verificar cria de userK", response.body()+ "");
                        Log.i("verificar cria de userK", response.body().toString()+ "");
                        Log.i("verificar cria de userK", response.isSuccess()+ "");
                        Log.i("verificar cria de userK", response.errorBody()+ "");
                        Log.i("verificar cria de userK", response.message()+"");
//                        try {
//                            //JSONObject data = new JSONObject(response.body());
//                            //Log.i("verificar cria de userK", data.toString()+"data");
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
                        //Data dataUser = response.body();
                        //Log.i("verificar cria de userK", dataUser.toString()+"");

                    }

                    @Override
                    public void onFailure(Throwable t) {
                        Log.i("verificar cria de userE", t.toString());
                    }
                });

            }
        });
    }
    


}
