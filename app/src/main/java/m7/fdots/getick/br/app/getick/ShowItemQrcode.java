package m7.fdots.getick.br.app.getick;

import android.app.Activity;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by geferson on 11/11/2015.
 */
public class ShowItemQrcode extends AppCompatActivity {

    private String title = "Código QR";

    @Bind(R.id.main_toolbar_item_qrcode)
    protected Toolbar toolbar;

    @Bind(R.id.code_qrcode_text)
    protected TextView cod_qrcode_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.showitemqrcode_activity);
        ButterKnife.bind(this);

        if(toolbar != null){
            toolbar.setTitle(title);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        if(getIntent().getExtras() != null){
            try{
                int position = (int) getIntent().getExtras().get("position");
                cod_qrcode_text.setText(String.format(getResources().getString(R.string.qr_codigo_text),position));

            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        int id = item.getItemId();
        if(id == android.R.id.home){
            finish();
        }
        return true;
    }
}
