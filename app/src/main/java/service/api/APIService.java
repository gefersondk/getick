package service.api;

import org.json.JSONObject;

import model.user.Data;
import model.user.UserGetick;
import model.event.DataEvent;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.POST;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by geferson on 15/11/2015.
 */
public interface APIService {

    @GET("events")
    Call<DataEvent> loadEvents();

    //@GET("events")
    //Call<DataEvent> loadNextEvents(@Query("page[number]") String numPagina,@Query("page[size]") String size ); //só teria que passar o interio  pra esses dois parametros  humcom o numero da pagina que tu quer e a quantidade de dados
    @GET("events")
    Call<DataEvent> loadNextEvents(@Query("page[number]") int numPagina, @Query("page[size]") int size);

    @GET("events")
    Call<DataEvent> loadCityEvents(@Query("search[city]") String city);

    @GET("events")
    Call<DataEvent> loadStateEvents(@Query("search[state]") String state);

    @POST("users")
    Call<Data> createUser(@Body UserGetick user);

    @POST("sessions")
    Call<String> login(String authentication_token);

    @DELETE("sessions")
    Call<String> logout(String authentication_token);

//    pra criar usuario tu da um POST para /api/users
//    pra logar tu dar um POST para /api/sessions
//    pra logout tu da um DELETE pra /api/sessions/{authentication_token}


}