package enumFragment;

import android.support.v4.app.Fragment;

import fragments.FragmentAccount;
import fragments.FragmentContainer;
import fragments.FragmentHome;
import fragments.FragmentTickets;

/**
 * Created by geferson on 01/11/2015.
 */
public enum FragmentSw {
    ticket {
        @Override
        public Fragment getFragment() {
            return new FragmentTickets();
        }
    },
    home_news {
        @Override
        public Fragment getFragment() {
            return new FragmentHome();
        }
    },
    events {
        @Override
        public Fragment getFragment() {//FragmentContainer();
            return new FragmentContainer();
        }
    },
//    points {
//        @Override
//        public Fragment getFragment() {
//            return new FragmentPoints();
//        }
//    },
    settings {
        @Override
        public Fragment getFragment() {
            return new FragmentAccount();
        }
    };

    public abstract Fragment getFragment();

}
