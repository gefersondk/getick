package facada;

import android.util.Log;

import model.config.GetickConfig;

/**
 * Created by geferson on 07/12/2015.
 */
public class GetickFacade {

    private static GetickFacade getickFacade;
    private GetickConfig getickConfig;

    private GetickFacade(){
        getickConfig = new GetickConfig();
    }

    public static synchronized GetickFacade getInstance(){
        if(getickFacade == null){
            getickFacade = new GetickFacade();
        }
        return getickFacade;
    }

    public GetickConfig getGetickConfig() {
        return getickConfig;
    }

}
