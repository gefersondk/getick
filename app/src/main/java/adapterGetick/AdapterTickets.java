package adapterGetick;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import m7.fdots.getick.br.app.getick.R;
import model.Tickte;

/**
 * Created by geferson on 10/11/2015.
 */
public class AdapterTickets extends RecyclerView.Adapter<AdapterTickets.ViewHolder>{

    private Context context;
    private List<Tickte> tickteList;
    private Activity activity;
    private LayoutInflater inflater;

    public AdapterTickets(Context context, List<Tickte> tickteList, Activity activity){
        this.context = context;
        this.tickteList = tickteList;
        this.activity = activity;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_ticket, parent, false);//parent é o recyclerview. false pra usar o layout params do recyclerview
        ViewHolder myViewHolder =  new ViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return tickteList.size();
    }

    public void addListItem(Tickte tickte, int position){
        tickteList.add(tickte);
        notifyItemInserted(position);
    }

    public void removeListItem(int position){
        tickteList.remove(position);
        notifyItemRemoved(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View itemView) {
            super(itemView);

        }
    }
}
