package adapterGetick;

/**
 * Created by Geferson on 30/07/2015.
 */

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.facebook.CallbackManager;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import butterknife.Bind;
import butterknife.ButterKnife;
import m7.fdots.getick.br.app.getick.MercadoPagoGetick;
import m7.fdots.getick.br.app.getick.R;
import model.event.DataObjectEvent;
import model.event.Event;


/**
 * Created by Geferson on 25/07/2015.
 */
public class AdapterCardViewEvent extends RecyclerView.Adapter<AdapterCardViewEvent.ViewHolderEvent> {
    ContentValues values;

    private List<DataObjectEvent> eventLis;
    private LayoutInflater inflater;
    public static Context context;
    private static Activity activity;
    private Typeface robotoLight;
    private Typeface robotoRegular;
    private Typeface robotoBold;
    private boolean animation;
    //private float scale;
    //private int width;
    //private int height;

    public AdapterCardViewEvent(Context context, List<DataObjectEvent> eventList, Activity activity, boolean animation){
        this.context = context;
        this.eventLis = eventList;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.activity = activity;
        robotoLight = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");//Roboto-Regular.ttf
        robotoRegular = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Regular.ttf");
        robotoBold = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Bold.ttf");
        this.animation = animation;
    }


    @Override
    public ViewHolderEvent onCreateViewHolder(ViewGroup parent, int viewType) {//chamada pesada precisa do bind para não precisar criar mais views

        View view = inflater.inflate(R.layout.cardview_event_simple, parent, false);//parent é o recyclerview. false pra usar o layout params do recyclerview
        ViewHolderEvent myViewHolderEvent =  new ViewHolderEvent(view);
        return myViewHolderEvent;
    }

    @Override
    public void onBindViewHolder(ViewHolderEvent holder, int position) {


        final String [] imgs = {"http://imagens.globoradio.globo.com/globoradio/fotosGen/6072/607174.jpg","http://www.vacarianews.com/wp-content/uploads/2015/01/facepromo.jpg","http://inteja.com.br/wp-content/uploads/2014/05/avioes-do-forro-inteja.jpg"};
        Random r = new Random();
        final int p = r.nextInt(3);
//        holder.maps.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(Intent.ACTION_VIEW);
//                intent.setData(Uri.parse("geo:19.076,72.8777"));
//                final Intent chooser = Intent.createChooser(intent, "Launch Maps");
//                context.startActivity(chooser);
//
//            }
//        });d

        Event event = eventLis.get(position).getAttributes();

        //holder.titleEventHouse.setText(event.getTitle());d
        //holder.titleEventHouse.setTypeface(robotoRegular);d
        //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy - HH:mm");d
        try {

            //String date = sdf.format(event.getEvent_date());d
            //holder.date.setText(date);d
            //holder.date.setTypeface(robotoRegular);d

        } catch (Exception e) {
            e.printStackTrace();
        }
        //holder.description_event.setText(Html.fromHtml("<p>" + event.getDescription() + "</p>"));d
        //holder.description_event.setTypeface(robotoLight);d

        //holder.bandName.setText(event.getAttractions());d
        //holder.bandName.setTypeface(robotoBold);d

        //holder.btn_share.setOnClickListener(new OptionDots("Baidu", "Domus Hall -  21/12/2015", "http://4dots.com.br/", imgs[p]));d



        if(position % 2 == 0){
            holder.photo.setImageURI(Uri.parse(imgs[p]));
        }else{
            holder.photo.setImageURI(Uri.parse(imgs[p]));
        }
        //Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
        if(animation){
            try{
                YoYo.with(Techniques.FadeInLeft)
                        .duration(700)
                        .playOn(holder.itemView);
            }catch(Exception e){
                e.printStackTrace();
            }
        }

        try{
            //holder.btn_buy.setOnClickListener(new MercadoPagoClickListener());
        }catch (Exception e){
           // e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return eventLis.size();
    }

    public void addListItem(DataObjectEvent dataObjectEvent, int position){
        eventLis.add(dataObjectEvent);
        notifyItemInserted(position);
    }

    public void removeListItem(int position){
        eventLis.remove(position);
        notifyItemRemoved(position);
    }

    public void configAnimation(){

    }

    public static class ViewHolderEvent extends RecyclerView.ViewHolder{

        @Bind(R.id.thumbnail_event)
        public SimpleDraweeView photo;

        public TextView name;
        public SimpleDraweeView img_pofile;

        public TextView status;//TODO MUDAR PARA STATUS NO SET DEPOIS

        //@Bind(R.id.id_event_house)
        //public TextView titleEventHouse;

        //@Bind(R.id.img_location)
        //public ImageView maps;

        //@Bind(R.id.card_date_id)
        //public TextView date;

        //@Bind(R.id.band_name)
        //public TextView bandName;

        //@Bind(R.id.card_btn_dots)
        //public ImageView btn_share;

        @Bind(R.id.btn_buy)
        public Button btn_buy;

       // @Bind(R.id.description_event)
        //public TextView description_event;

        public ViewHolderEvent(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    protected class OptionDots implements View.OnClickListener{

        protected String title, description, url, img_url;

        public OptionDots(String title, String description, String url, String img_url){

            this.title = title;
            this.description = description;
            this.url = url;
            this.img_url = img_url;

        }

        @Override
        public void onClick(View v) {
            final CharSequence[] items = {"Publicar no Facebook"};

            new MaterialDialog.Builder(activity)
                    .title("Getick")
                    //.content(description)
                    .positiveText("Sair")
                            //.iconRes(R.drawable.profile2)
                    .icon(context.getResources().getDrawable(R.drawable.ic_activity))
                    .backgroundColorRes(android.R.color.white)
                    .maxIconSize(100)
                            .titleColorRes(R.color.colorPrimaryDark)
                                    .contentColorRes(R.color.fontL)
                            //.negativeText("vermelho")
                    .items(R.array.OptionDots)
                    .itemsCallback(new MaterialDialog.ListCallback() {
                        @Override
                        public void onSelection(MaterialDialog materialDialog, View view, int i, CharSequence charSequence) {
                            if (charSequence.equals("Publicar no Facebook")) {

                                CallbackManager callbackManager = CallbackManager.Factory.create();
                                ShareDialog shareDialog = new ShareDialog(activity);
                                if (ShareDialog.canShow(ShareLinkContent.class)) {
                                    ShareLinkContent linkContent = new ShareLinkContent.Builder()
                                            .setContentTitle(title)
                                            .setContentDescription(
                                                    description)
                                            .setContentUrl(Uri.parse(url))
                                            .setImageUrl(Uri.parse(img_url))
                                            .build();

                                    shareDialog.show(linkContent);
                                }

                            } else if (charSequence.equals("Localização")) {
                                materialDialog.dismiss();
                            }
                        }
                    })
                    .show();
        }
    }

    protected class MercadoPagoClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {

            context.startActivity(new Intent(context, MercadoPagoGetick.class));

        }
    }
}
